package cli

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCheckHTTP(t *testing.T) {
	assert.True(t, checkHTTP("http://example.com"))
	assert.True(t, checkHTTP("https://example.com"))
	assert.False(t, checkHTTP(""))
}

func TestValidateJSON(t *testing.T) {
	validJ := []byte(`{"this":"is", "valid": "json"}`)
	invalidJ := []byte(`{"this":"is", invalid: "json"}`)
	assert.True(t, validateJSON(validJ))
	assert.False(t, validateJSON(invalidJ))
}
