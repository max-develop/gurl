package cli

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/urfave/cli"
)

var (
	// Header ...
	Header bool
	// SpecificHeader ...
	SpecificHeader string
	// ClientBody ...
	ClientBody string
	// ClientFile ...
	ClientFile string
)

var (
	commands = []cli.Command{
		getCommand(),
		headCommand(),
		postCommand(),
		putCommand(),
		deleteCommand(),
	}

	// Slice of flags for methods without body
	flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "header",
			Usage:       "Get the Header in addition to the body",
			Destination: &Header,
		},
		cli.StringFlag{
			Name:        "specific,sh",
			Usage:       "Get a specific header",
			Destination: &SpecificHeader,
		},
	}

	// Slice of flags for methods with a body
	bodyFlags = []cli.Flag{
		cli.BoolFlag{
			Name:        "header",
			Usage:       "Get the header in addition to the body",
			Destination: &Header,
		},
		cli.StringFlag{
			Name:        "specific,sh",
			Usage:       "Get a specific header",
			Destination: &SpecificHeader,
		},
		cli.StringFlag{
			Name:        "body,b",
			Usage:       "Set your payload",
			Destination: &ClientBody,
		},
		cli.StringFlag{
			Name:        "file,f",
			Usage:       "Set your payload from `FILE`",
			Destination: &ClientFile,
		},
	}
)

func printHeader(resp *http.Response) {
	for name, values := range resp.Header {
		fmt.Printf("%s: %s\n", name, values)
	}
	fmt.Printf("\n")
}

func printSpecificHeader(resp *http.Response, Header string) {
	result := resp.Header.Get(Header)
	if result == "" {
		fmt.Println("Server did not specify this header:", Header)
		return
	}
	fmt.Println(result)
}

func getFirstArg(c *cli.Context) string {
	URL := c.Args().First()
	if !checkHTTP(URL) {
		URL = fmt.Sprintf("http://%s", URL)
	}
	if "" == "" {

	}
	return URL
}

func checkHTTP(url string) bool {
	return strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://")
}

func validateJSON(input []byte) bool {
	var js map[string]interface{}
	return json.Unmarshal(input, &js) == nil
}
