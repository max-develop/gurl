package cli

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/baroprime/gurl/cli/runtime"
)

var (
	app        *cli.App
	configPath string
	extraArg   string
)

func init() {
	app = cli.NewApp()
	app.Name = "gurl"
	app.Usage = "HTTP client written in golang"
	app.Version = "1.0"
	app.Commands = commands
}

// RunCLI gathers command line arguments and runs the CLI application
func RunCLI(osArgs []string, opts ...runtime.AppOption) error {
	opts = append(opts, runtime.OsArguments(osArgs))

	rt, err := runtime.New(opts...)
	if err != nil {
		fmt.Printf("unable to initialize CLI: %v", err)
		os.Exit(1)
	}

	//return runtime.Run(args)
	return rt.Run(app)
}
