package cli

import (
	"log"

	"github.com/urfave/cli"
	"gitlab.com/baroprime/gurl/pkg/clients"
)

func headCommand() cli.Command {
	return cli.Command{
		Name:  "head",
		Usage: "send a HEAD request to the given URL",
		Action: func(c *cli.Context) error {
			URL := getFirstArg(c)

			client := clients.NewClient()

			resp, err := client.Get(URL)
			if err != nil {
				log.Println("Could not get response")
				return nil
			}

			if resp != nil {
				if SpecificHeader != "" {
					printSpecificHeader(resp, SpecificHeader)
					return nil
				}
				printHeader(resp)
			}
			return nil
		},
		Flags: flags,
	}
}
