package cli

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/urfave/cli"
	"gitlab.com/baroprime/gurl/pkg/clients"
)

func postCommand() cli.Command {
	return cli.Command{Name: "post",
		Usage: "send a POST request to the given URL",
		Action: func(c *cli.Context) error {
			URL := getFirstArg(c)

			client := clients.NewClient()

			payload := ClientBody
			if payload == "" {
				file, err := ioutil.ReadFile(ClientFile)
				if err != nil {
					log.Println("Could not read file")
					return nil
				}
				payload = string(file)
			}

			resp, err := client.Post(URL, payload)
			if err != nil {
				log.Println("Could not get response")
				return nil
			}

			if resp != nil {
				if SpecificHeader != "" {
					printSpecificHeader(resp, SpecificHeader)
					return nil
				}
				if Header {
					printHeader(resp)
				}
				data, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Println("Could not read response body")
					return nil
				}
				fmt.Println(string(data))
				return nil
			}
			return nil
		},
		Flags: bodyFlags,
	}
}
