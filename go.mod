module gitlab.com/baroprime/gurl

go 1.12

require (
	github.com/stretchr/testify v1.4.0
	github.com/urfave/cli v1.21.0
)
