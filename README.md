# gURL - CLI HTTP client written in golang
Fork from [l3x/mycli](https://github.com/l3x/mycli).


## How to use it

| Method | Command | Parameters|
|---|---|---|
|GET |`gurl get` | http://website/path/to/ressource| 
|HEAD |`gurl head` | http://website/path/to/ressource| 
|POST |`gurl post` | http://website/path/to/ressource| 
|PUT|`gurl put` | http://website/path/to/ressource| 
|DELETE |`gurl delete` | http://website/path/to/ressource| 

### Options

##### All
- `--Header` returns the response Header
- `--specific, -sh` returns the given Header field, if it was returned from the server
##### with body
- `--body, -b` sets a given body
- `--file, -f` sets the body from the given file

