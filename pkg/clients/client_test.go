package clients

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestCreateRequest tests createRequest
func TestCreateRequest(t *testing.T) {
	request := createRequest("GET", "https://example.com", "")
	assert.Equal(t, request.Method, "GET", "Request Method should be equal")
	assert.Equal(t, request.URL.Scheme, "https", "URL Scheme should be equal")
	assert.Equal(t, request.URL.Host, "example.com", "URL Host should be equal")

	request = createRequest("", "https://example.com", "")
	assert.Nil(t, request)

	request = createRequest("POST", "", "")
	assert.Nil(t, request)

	request = createRequest("", "", "")
	assert.Nil(t, request)
}
