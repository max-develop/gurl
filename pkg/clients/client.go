package clients

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

//Client implemewnts an client object
type Client struct {
	Client http.Client
}

//NewClient returns an empty Client object
func NewClient() Client {
	return Client{http.Client{}}
}

// Get sends a GET request
func (c *Client) Get(URL string) (*http.Response, error) {
	req := createRequest("GET", URL, "")
	return c.sendRequest(req), nil
}

// Head sends a HEAD request
func (c *Client) Head(URL string) (*http.Response, error) {
	req := createRequest("HEAD", URL, "")
	return c.sendRequest(req), nil
}

// Post sends a POST request
func (c *Client) Post(URL string, body string) (*http.Response, error) {
	req := createRequest("POST", URL, body)
	return c.sendRequest(req), nil
}

// Put sends a PUT request
func (c *Client) Put(URL string, body string) (*http.Response, error) {
	req := createRequest("PUT", URL, body)
	return c.sendRequest(req), nil
}

// Delete sends a DELETE request
func (c *Client) Delete(URL string, body string) (*http.Response, error) {
	req := createRequest("DELETE", URL, body)
	return c.sendRequest(req), nil
}

//create request from user input
func createRequest(method string, URL string, body string) *http.Request {
	if method == "" {
		fmt.Println("No Method given")
		return nil
	}
	if URL == "" {
		fmt.Println("No URL given")
		return nil
	}
	if body != "" && (method == "GET" || method == "HEAD") {
		fmt.Println("No body required")
		return nil
	}
	req, err := http.NewRequest(method, URL, strings.NewReader(string(body)))
	if err != nil {
		log.Printf("[ERROR] Cannot create request: %s \n", err)
		return nil
	}
	return req
}

//send the request
func (c *Client) sendRequest(req *http.Request) *http.Response {
	resp, err := c.Client.Do(req)
	if err != nil {
		log.Printf("[ERROR] Cannot send request: %s \n", err)
		return nil
	}
	return resp
}
