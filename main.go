package main

import (
	"log"
	"os"

	"gitlab.com/baroprime/gurl/cli"
)

var (
	// Version is the semantic version (added at compile time)  See scripts/version.sh
	Version string

	// Revision is the git commit id (added at compile time)
	Revision string
)

func main() {

	cliErr := cli.RunCLI(os.Args)
	if cliErr != nil {
		log.Println("CLI error encountered:")
		log.Fatal(cliErr)
	}

}
